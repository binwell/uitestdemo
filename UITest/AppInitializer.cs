﻿using System;
using System.IO;
using System.Linq;
using Xamarin.UITest;
using Xamarin.UITest.Queries;

namespace UITest
{
    public class AppInitializer
    {

        
        const string ApkFile = "../../../UiTestExample/UiTestExample.Android/bin/Release/UiTestExample.Android-Signed.apk";
        const string Appfile = "/Users/admin/Desktop/UiTestExample/UiTestExample/UiTestExample.iOS/bin/iPhoneSimulator/Release/UiTestExample.iOS.app";
      

        private static IApp app;

        public static IApp App
        {
            get
            {
                if (app == null)
                    throw new NullReferenceException("AppInitializer.App' not set.");
                return app;
            }
        }

        public static IApp StartApp(Platform platform)
        {
            if (platform == Platform.Android)
            {
                app = ConfigureApp.Android.ApkFile(ApkFile)
                    .StartApp(Xamarin.UITest.Configuration.AppDataMode.Clear);
            }
            else
            {
                app = ConfigureApp.iOS.AppBundle(Appfile)
                                  .DeviceIdentifier("4A802B8E-77C3-4362-BA49-C69845EF093C")
                    .StartApp(Xamarin.UITest.Configuration.AppDataMode.Clear);
            }

            return app;
        }
    }
}

