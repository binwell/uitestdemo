﻿namespace UITest
{
    public class ButtonPage : BasePage
    {
        public ButtonPage TapOnButton()
        {

            app.Tap("Button");
            if (OnAndroid)
            {
                app.WaitForElement("Title");
            }
            if (OniOS)
            {
                app.WaitForElement("Title");
            }
            app.Screenshot("TapButton");

            return this;
        }
    }
}

