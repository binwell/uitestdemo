﻿namespace UITest
{
    public class EntryPage : BasePage
    {
        public EntryPage TapField()
        {

            app.Tap("Entry");
            app.Screenshot("TapField");

            return this;
        }

        public EntryPage EnterText()
        {

            app.EnterText("ExmpleEnterText");
            app.DismissKeyboard();
            app.Screenshot("EnterText");

            return this;
        }
    }
}

