﻿namespace UITest
{
    public class MainPage : BasePage
    {
        public MainPage TapListPage()
        {

            app.Tap("OpenListPage");
            app.Screenshot("OpenListPage");

            return this;
        }

        public MainPage TapEntryPage()
        {

            app.Tap("OpenEntryPage");
            app.Screenshot("OpenEntryPage");

            return this;
        }

        public MainPage TapButtonPage()
        {
            app.Tap("OpenButtonPage");
            app.Screenshot("OpenButtonPage");

            return this;
        }
    }
}

