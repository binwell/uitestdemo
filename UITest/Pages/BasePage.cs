﻿using System;
using Xamarin.UITest;
using Xamarin.UITest.Queries;
using Xamarin.UITest.Android;
using Xamarin.UITest.iOS;

namespace UITest
{
    public partial class BasePage
    {
        protected readonly IApp app;
        protected readonly bool OnAndroid;
        protected readonly bool OniOS;

        protected Func<AppQuery, AppQuery> Trait;
        Platform platform;

        protected BasePage()
        {
            app = AppInitializer.App;

            OnAndroid = app.GetType() == typeof(AndroidApp);
            OniOS = app.GetType() == typeof(iOSApp);
        }

        public BasePage(Platform platform) : this()
        {
            this.platform = platform;
        }


    }
}
