﻿namespace UITest
{
    public class ListViewPage : BasePage
    {
        public ListViewPage Scroll()
        {

            app.Screenshot("BeforeScroll");
            app.ScrollDown();
            app.Screenshot("AfterScroll");

            return this;
        }
    }
}

