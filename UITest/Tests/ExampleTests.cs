﻿using NUnit.Framework;
using Xamarin.UITest;


namespace UITest
{
    public class ExampleTests : BaseTest
    {
        public ExampleTests(Platform platform)
            : base(platform)
        {
        }

        [Test]

        public void Repl()
        {

            app.Repl();

        }


        [Test]
        public void ButtonPage()
        {
            new MainPage()
                .TapButtonPage();

            new ButtonPage()
                .TapOnButton();
        }

        [Test]

        public void EntryPage()
        {
            new MainPage()
                .TapEntryPage();

            new EntryPage()
                .TapField()
                .EnterText();
        }

        [Test]
        public void ListViewPage()
        {
            new MainPage()
                .TapListPage();

            new ListViewPage()
                .Scroll();

        }
    }
}
