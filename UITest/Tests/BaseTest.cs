﻿using Xamarin.UITest;
using NUnit.Framework;
using Xamarin.UITest.Android;
using Xamarin.UITest.iOS;

namespace UITest
{
    [TestFixture(Platform.Android)]
    //[TestFixture(Platform.iOS)]
    public abstract class BaseTest
    {
        protected IApp app;
        protected Platform platform;
        protected bool OnAndroid { get; set; }
        protected bool OniOS { get; set; }
        public BasePage Page { get; private set; }


        public BaseTest(Platform platform)
        {
            this.platform = platform;
        }


        [SetUp]
        public virtual void BeforeEachTest()
        {
            app = AppInitializer.StartApp(platform);
            OnAndroid = app.GetType() == typeof(AndroidApp);
            OniOS = app.GetType() == typeof(iOSApp);
            Page = new BasePage(platform);
        }
    }
}


