﻿using System;
using System.Collections.Generic;
 

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace UiTestExample
{

    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ListViewPage : ContentPage
    {
        public ListViewPage()
        {
            InitializeComponent();
            ListView.ItemsSource = GetItemsSource();
        }


        List<object> GetItemsSource()
        {
            var list = new List<object>();
            for (var i = 0; i < 20; i++)
            {
                list.Add(new {Text = "Item " + i});
            }
            return list;
        }
    }
}
