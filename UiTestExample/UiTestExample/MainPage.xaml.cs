﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace UiTestExample
{
    public partial class MainPage : ContentPage
    {
        public MainPage()
        {
            InitializeComponent();
        }

        private void OpenButtonPage_OnTapped(object sender, EventArgs e)
        {
            Application.Current.MainPage.Navigation.PushModalAsync(new NavigationPage(new ButtonPage()));
        }

        private void OpenEntryPage_OnTapped(object sender, EventArgs e)
        {
            Application.Current.MainPage.Navigation.PushModalAsync(new NavigationPage(new EntryPage()));
        }
        private void OpenListPage_OnTapped(object sender, EventArgs e)
        {
            Application.Current.MainPage.Navigation.PushModalAsync(new NavigationPage(new ListViewPage()));
        }
    }
}
