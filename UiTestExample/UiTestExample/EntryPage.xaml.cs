﻿
using System;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace UiTestExample
{

    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class EntryPage : ContentPage
    {
        public EntryPage()
        {
            InitializeComponent();
         }

        private async void Entry_OnCompleted(object sender, EventArgs e)
        {
            await Application.Current.MainPage.DisplayAlert("Title", "Entry Completed!", "Ok", "Cancel");
        }
    }

 }
